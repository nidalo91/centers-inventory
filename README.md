## Centers inventory

This application is a simple inventory management system for centers.  
- As a **regular user** you view centers, products and their inventory. You can also create, accept, reject and cancel transfers between your center and other centers.  
- As an **admin** you can create, edit and delete centers, products, transfers and users.

### Demo

You can see a demo of this application at [https://still-atoll-74840.herokuapp.com](https://still-atoll-74840.herokuapp.com)

To login as an admin use the following credentials:  
- **login**: admin  
- **password**: password

To login as a user use the following credentials:  
- **login**: user  
- **password**: password

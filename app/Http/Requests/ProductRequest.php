<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\ProductCategory;
use Illuminate\Foundation\Http\FormRequest;

final class ProductRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        $categories = implode(',', array_column(ProductCategory::cases(), 'name'));

        return [
            'name' => 'required|string|max:255',
            'price' => 'required|numeric',
            'category' => "required|string|in:$categories",
        ];
    }
}

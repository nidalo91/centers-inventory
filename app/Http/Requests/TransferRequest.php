<?php

declare(strict_types=1);

namespace App\Http\Requests;

use App\Enums\TransferStatus;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest;

final class TransferRequest extends FormRequest
{
    public function authorize(): bool
    {
        return (int) $this->sender_id === auth()->user()->center_id || auth()->user()->isAdmin();
    }

    public function rules(): array
    {
        $statuses = implode(',', array_column(TransferStatus::cases(), 'name'));

        return [
            'sender_id' => 'required|exists:centers,id',
            'receiver_id' => 'required|exists:centers,id|different:sender_id',
            'product_id' => "required|exists:products,id|exists:center_product,product_id,center_id,{$this->sender_id}",
            'quantity' => 'required|numeric|min:1',
            'status' => "in:$statuses",
        ];
    }

    public function failedAuthorization()
    {
        throw new AuthorizationException('You are not allowed to create transfers for other centers.');
    }
}

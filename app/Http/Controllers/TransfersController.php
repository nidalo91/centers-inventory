<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enums\TransferStatus;
use App\Http\Requests\TransferRequest;
use App\Models\Center;
use App\Models\Product;
use App\Models\Transfer;
use App\Services\TransferService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class TransfersController extends Controller
{
    public function index(): View
    {
        $transfers = Transfer::orderBy('created_at', 'desc')->paginate(10);

        return view('transfers.index', compact('transfers'));
    }

    public function create(Request $request): View
    {
        $centerId = $request->query('center_id');
        $productId = $request->query('product_id');

        $centers = Center::all();
        $products = Product::all();

        return view(
            'transfers.create',
            compact('centerId', 'productId', 'centers', 'products')
        );
    }

    public function store(TransferRequest $request): RedirectResponse
    {
        $center = Center::find($request->sender_id)->load([
            'products' => fn ($query) =>  $query->where('product_id', $request->product_id)
        ]);

        $product = $center->products->first();

        if ($product->pivot->quantity < (int) $request->quantity) {
            return redirect()
                ->back()
                ->withInput()
                ->withErrors(['quantity' => "Not enough products in the sender center. Available quantity is {$product->pivot->quantity}."]);
        }

        Transfer::create($request->validated());

        return redirect()
            ->route('transfers.index')
            ->with('message', 'Transfer created successfully.');
    }

    public function update(Transfer $transfer, Request $request): RedirectResponse
    {
        $transfer->update(['status' => $request->status]);

        if ($request->status === TransferStatus::ACCEPTED->name) {
            (new TransferService())->updateQuantity($transfer);
        }

        return redirect()
            ->route('transfers.index')
            ->with('message', 'Transfer status updated successfully.');
    }
}

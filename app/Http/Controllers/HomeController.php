<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Contracts\View\View;

class HomeController extends Controller
{
    public function __invoke(): View
    {
        $mainCenter = auth()->user()->center;

        return view('home', compact('mainCenter'));
    }
}

<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Enums\ProductCategory;
use App\Http\Middleware\Admin;
use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware(Admin::class)->only(['create', 'store', 'edit', 'update', 'destroy']);
    }

    public function index(): View
    {
        $products = Product::orderBy('name')->paginate(10);

        return view('products.index', compact('products'));
    }

    public function show(Product $product): View
    {
        return view('products.show', compact('product'));
    }

    public function create(): View
    {
        $categories = ProductCategory::cases();

        return view('products.create', compact('categories'));
    }

    public function store(ProductRequest $request): RedirectResponse
    {
        Product::create($request->validated());

        return redirect()
            ->route('products.index')
            ->with('message', 'Product created successfully');
    }

    public function edit(Product $product): View
    {
        $categories = ProductCategory::cases();

        return view('products.edit', compact('product', 'categories'));
    }

    public function update(ProductRequest $request, Product $product): RedirectResponse
    {
        $product->update($request->validated());

        return redirect()
            ->route('products.index')
            ->with('message', 'Product updated successfully');
    }

    public function destroy(Product $product): RedirectResponse
    {
        $product->delete();

        return redirect()
            ->route('products.index')
            ->with('message', 'Product deleted successfully');
    }
}

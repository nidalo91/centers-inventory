<?php

declare(strict_types=1);

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class LoginController extends Controller
{
    public function show(): View
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request): RedirectResponse
    {
        if (false === auth()->attempt($request->validated())) {
            return back()
                ->withInput()
                ->withErrors(['login' => 'The provided credentials do not match our records.']);
        }

        session()->regenerate();

        return redirect()->intended();
    }
}

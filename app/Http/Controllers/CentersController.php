<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Middleware\Admin;
use App\Http\Requests\CenterRequest;
use App\Models\Center;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CentersController extends Controller
{
    public function __construct()
    {
        $this->middleware(Admin::class)->only(['create', 'store']);
    }

    public function index(): View
    {
        $centers = Center::orderBy('name')->get();

        return view('centers.index', compact('centers'));
    }

    public function show(Center $center): View
    {
        return view('centers.show', compact('center'));
    }

    public function create(): View
    {
        return view('centers.create');
    }

    public function store(CenterRequest $request): RedirectResponse
    {
        Center::create($request->validated());

        return redirect()
            ->route('centers.index')
            ->with('message', 'Center created successfully');
    }

    public function edit(Center $center): View
    {
        return view('centers.edit', compact('center'));
    }

    public function update(CenterRequest $request, Center $center): RedirectResponse
    {
        $center->update($request->validated());

        return redirect()
            ->route('centers.index')
            ->with('message', 'Center updated successfully');
    }

    public function destroy(Center $center): RedirectResponse
    {
        $center->delete();

        return redirect()
            ->route('centers.index')
            ->with('message', 'Center deleted successfully');
    }
}

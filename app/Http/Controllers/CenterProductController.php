<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Requests\CenterProductRequest;
use App\Models\Center;
use App\Models\Product;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CenterProductController extends Controller
{
    public function create(Center $center): View
    {
        $centerProducts = $center->products()->pluck('product_id')->toArray();

        $products = Product::whereNotIn('id', $centerProducts)->orderBy('name')->get();

        return view('centers.products.create', compact('center', 'products'));
    }

    public function store(CenterProductRequest $request, Center $center): RedirectResponse
    {
        $center->products()->attach($request->product_id, ['quantity' => $request->quantity]);

        return redirect()
            ->route('centers.show', $center)
            ->with('message', 'Product added to center successfully');
    }

    public function edit(Center $center, Product $product): View
    {
        $pivot = $center->products()->where('product_id', $product->id)->first()->pivot;

        return view('centers.products.edit', compact('center', 'product', 'pivot'));
    }

    public function update(CenterProductRequest $request, Center $center, Product $product): RedirectResponse
    {
        $center->products()->updateExistingPivot($product->id, ['quantity' => $request->quantity]);

        return redirect()
            ->route('centers.show', $center)
            ->with('message', 'Product quantity updated in center successfully');
    }

    public function destroy(Center $center, Product $product): RedirectResponse
    {
        $center->products()->detach($product);

        return redirect()
            ->route('centers.show', $center)
            ->with('message', 'Product removed from center successfully');
    }
}

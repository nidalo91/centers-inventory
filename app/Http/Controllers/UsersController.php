<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Http\Middleware\Admin;
use App\Http\Requests\UserRequest;
use App\Models\Center;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware(Admin::class)->only(['index', 'create', 'store', 'destroy']);
    }
    public function index(): View
    {
        $users = User::all();

        return view('users.index', compact('users'));
    }

    public function create(): View
    {
        $centers = Center::all();

        return view('users.create', compact('centers'));
    }

    public function store(UserRequest $request): RedirectResponse
    {
        User::create($request->validated());

        return redirect()
            ->route('users.index')
            ->with('message', 'User created successfully');
    }

    public function destroy(User $user): RedirectResponse
    {
        $user->delete();

        return redirect()
            ->route('users.index')
            ->with('message', 'User deleted successfully');
    }
}

<?php

namespace App\Console\Commands;

use App\Enums\TransferStatus;
use App\Models\Transfer;
use Illuminate\Console\Command;

class CancelTransferCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'transfers:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel transfers that are older than 24 hours';

    public function handle(): void
    {
        $this->info('Canceling transfers...');

        $transfers = Transfer::where('created_at', '<', now()->subDay())->get();
        $transfers->each->update(['status' => TransferStatus::CANCELED->name]);

        $this->info("{$transfers->count()} Transfers canceled");
    }
}

<?php

declare(strict_types=1);

namespace App\Services;

use App\Models\Center;
use App\Models\Product;
use App\Models\Transfer;

class TransferService
{
    public function updateQuantity(Transfer $transfer): void
    {
        $sender = Center::find($transfer->sender_id);
        $receiver = Center::find($transfer->receiver_id);
        $product = Product::find($transfer->product_id);

        $senderProduct = $sender->products()->find($product->id);
        $receiverProduct = $receiver->products()->find($product->id);

        $sender->products()->updateExistingPivot($product->id, [
            'quantity' => $senderProduct->pivot->quantity - $transfer->quantity,
        ]);

        if ($receiverProduct === null) {
            $receiver->products()->attach($product->id, [
                'quantity' => $transfer->quantity,
            ]);
        } else {
            $receiver->products()->updateExistingPivot($product->id, [
                'quantity' => $receiverProduct->pivot->quantity + $transfer->quantity,
            ]);
        }

        if ($senderProduct->pivot->quantity === $transfer->quantity) {
            $sender->products()->detach($product->id);
        }
    }
}

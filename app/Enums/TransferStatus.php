<?php

declare(strict_types=1);

namespace App\Enums;

enum TransferStatus
{
    case PENDING;
    case ACCEPTED;
    case REJECTED;
    case CANCELED;
}

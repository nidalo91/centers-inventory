<?php

declare(strict_types=1);

namespace App\Enums;

enum ProductCategory: string
{
    case FOOD = 'Food';
    case DRINK = 'Drink';
    case ELECTRONICS = 'Electronics';
    case CLOTHES = 'Clothes';
    case OTHER = 'Other';
}

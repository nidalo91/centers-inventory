<?php

declare(strict_types=1);

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'price',
        'category',
    ];

    public function transfers(): HasMany
    {
        return $this->hasMany(Transfer::class);
    }

    public function centers(): BelongsToMany
    {
        return $this->belongsToMany(Center::class)->withPivot('quantity');
    }
}

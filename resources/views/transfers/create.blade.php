@extends('layouts.app')

@section('title', "Create a transfer")

@section('content')
    <form action="{{ route('transfers.store') }}" method="POST">
        @csrf

        <div class="mb-3">
            <label for="sender_id" class="form-label">From</label>
            <select class="form-select" id="sender_id" name="sender_id">
                @foreach($centers as $center)
                    @if($center->id == $centerId)
                        <option value="{{ $center->id }}" selected>{{ $center->name }}</option>
                    @else
                        <option value="{{ $center->id }}" {{ $center->id == old('sender_id') ? 'selected' : '' }}>{{ $center->name }}</option>
                    @endif
                @endforeach
            </select>
            @error('sender_id')
            <div class="alert alert-danger mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-3">
            <label for="receiver_id" class="form-label">To</label>
            <select class="form-select" id="receiver_id" name="receiver_id">
                @foreach($centers as $center)
                    <option value="{{ $center->id }}" {{ $center->id == old('receiver_id') ? 'selected' : '' }}>{{ $center->name }}</option>
                @endforeach
            </select>
            @error('receiver_id')
            <div class="alert alert-danger mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-3">
            <label for="product_id" class="form-label">Product</label>
            <select class="form-select" id="product_id" name="product_id">
                @foreach($products as $product)
                    @if($product->id == $productId)
                        <option value="{{ $product->id }}" selected>{{ $product->name }}</option>
                    @else
                        <option value="{{ $product->id }}" {{ $product->id == old('product_id') ? 'selected' : '' }}>{{ $product->name }}</option>
                    @endif
                @endforeach
            </select>
            @error('product_id')
            <div class="alert alert-danger mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-3">
            <label for="quantity" class="form-label">Quantity</label>
            <input type="number" class="form-control" id="quantity" name="quantity" value="{{ old('quantity') }}">
            @error('quantity')
            <div class="alert alert-danger mt-2">{{ $message }}</div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Create transfer</button>
    </form>
@endsection

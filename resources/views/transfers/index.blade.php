@extends('layouts.app')

@section('title', "Transfers ({$transfers->total()})")

@section('content')
    <a class="btn btn-primary float-end mb-4" href="{{ route('transfers.create') }}">Create a new transfer</a>

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

    <table class="table">
        <thead>
            <tr>
                <th>Product</th>
                <th>Quantity</th>
                <th>From</th>
                <th>To</th>
                <th>Status</th>
                <th>Created at</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($transfers as $transfer)
                <tr @if($transfer->status === 'CANCELLED') class="table-secondary" @endif>
                    <td>{{ $transfer->product->name }}</td>
                    <td>{{ $transfer->quantity }}</td>
                    <td>{{ $transfer->sender->name }}</td>
                    <td>{{ $transfer->receiver->name }}</td>
                    <td>{{ $transfer->status }}</td>
                    <td>{{ $transfer->created_at }}</td>
                    <td>
                        @if(
                            $transfer->receiver->id === auth()->user()->center->id
                            || $transfer->sender->id === auth()->user()->center->id
                            || auth()->user()->isAdmin()
                        )
                            @if($transfer->status === 'PENDING')
                                @if($transfer->sender->id !== auth()->user()->center->id || auth()->user()->isAdmin())
                                    <form class="d-inline" action="{{ route('transfers.update', $transfer->id) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <input type="hidden" name="status" value="ACCEPTED">
                                        <input class="btn btn-outline-success btn-sm" type="submit" value="Accept">
                                    </form>

                                    <form class="d-inline" action="{{ route('transfers.update', $transfer->id) }}" method="POST">
                                        @csrf
                                        @method('PUT')
                                        <input type="hidden" name="status" value="REJECTED">
                                        <input class="btn btn-outline-danger btn-sm" type="submit" value="Reject">
                                    </form>
                                @endif

                                <form class="d-inline" action="{{ route('transfers.update', $transfer->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <input type="hidden" name="status" value="CANCELLED">
                                    <input class="btn btn-outline-info btn-sm" type="submit" value="Cancel">
                                </form>
                            @endif
                        @else
                            <span class="text-sm-center">Your are not allowed.</span>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $transfers->links("pagination::bootstrap-5") }}
@endsection

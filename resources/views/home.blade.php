@extends('layouts.app')

@section('title', "Welcome to your center ({$mainCenter->name}) inventory!")

@section('content')
    <hr>
    <div class="row">
        <div class="col-md-4">
            <h4>Nb of products</h4>
            <p>{{ $mainCenter->products->count() }}</p>
            <a class="btn btn-primary" href='{{ route('centers.show', $mainCenter->id) }}'>Go to products</a>
        </div>
        <div class="col-md-4">
            <h4>Nb of received transfers</h4>
            <p>{{ $mainCenter->receivedTransfers->count() }}</p>
            <a class="btn btn-primary" href='{{ route('transfers.index') }}'>Go to received transfers</a>
        </div>
        <div class="col-md-4">
            <h4>Nb of center users</h4>
            <p>{{ $mainCenter->users->count() }}</p>
        </div>
    </div>
@endsection

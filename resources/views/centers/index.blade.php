@extends('layouts.app')

@section('title', "Centers ({$centers->count()})")

@section('content')
    @if(auth()->user()->isAdmin())
        <a class="btn btn-primary float-end mb-4" href="{{ route('centers.create') }}">Create a new center</a>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Address</th>
                <th>City</th>
                <th>State</th>
                <th>Zip</th>
                <th>Products</th>
                <th>Users</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($centers as $center)
                <tr>
                    <td>{{ $center->name }}</td>
                    <td>{{ $center->address }}</td>
                    <td>{{ $center->city }}</td>
                    <td>{{ $center->state }}</td>
                    <td>{{ $center->zip }}</td>
                    <td><span class="badge rounded-pill bg-success">{{ $center->products->count() }}</span></td>
                    <td><span class="badge rounded-pill bg-success">{{ $center->users->count() }}</span></td>
                    <td>
                        <a class="btn btn-outline-secondary btn-sm" href="{{ route('centers.show', $center->id) }}">Show</a>
                        @if($center->id === auth()->user()->center->id || auth()->user()->isAdmin())
                            <a class="btn btn-secondary btn-sm" href="{{ route('centers.edit', $center->id) }}">Edit</a>
                            <form class="d-inline" action="{{ route('centers.destroy', $center->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-danger btn-sm" type="submit" value="Delete">
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

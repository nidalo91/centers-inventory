@extends('layouts.app')

@section('title', ucfirst($center->name))

@section('content')
    <p>{{ $center->address }}</p>
    <p>{{ $center->city }}, {{ $center->state }} {{ $center->zip }}</p>

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

    @if($center->id === auth()->user()->center->id || auth()->user()->isAdmin())
        <a class="btn btn-primary float-end mb-4 me-2" href="{{ route('centers.products.create', $center->id) }}">Add a new product</a>
    @endif

    <h4>Products ({{ $center->products->count() }})</h4>
    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($center->products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->price }} €</td>
                    <td>{{ $product->pivot->quantity }}</td>
                    <td>
                        <a class="btn btn-outline-secondary btn-sm" href="{{ route('products.show', $product->id) }}">Show</a>
                        @if($center->id === auth()->user()->center->id || auth()->user()->isAdmin())
                            <a class="btn btn-success btn-sm" href="{{ route('centers.products.edit', [$center->id, $product->id]) }}">Update quantity</a>
                            <a class="btn btn-info btn-sm" href="{{ route('transfers.create', ['center_id' => $center->id, 'product_id' => $product->id]) }}">Transfer</a>
                            <form class="d-inline" action="{{ route('centers.products.destroy', [$center->id, $product->id]) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-danger btn-sm" type="submit" value="Remove">
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

@extends('layouts.app')

@section('title', "Update {$product->name}'s quantity for {$center->name}")

@section('content')
    <form action="{{ route('centers.products.update', [$center->id, $product->id]) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="mb-3">
            <label for="product_id" class="form-label">Product</label>
            <select class="form-select" id="product_id" name="product_id">
                <option value="{{ $product->id }}" {{ $product->id === old('product_id') ? 'selected' : '' }}>{{ $product->name }}</option>
            </select>
            @error('product_id')
            <div class="alert alert-danger mt-2">{{ $message }}</div>
            @enderror
        </div>

        <div class="mb-3">
            <label for="quantity" class="form-label">Quantity</label>
            <input type="number" class="form-control" id="quantity" name="quantity" value="{{ old('quantity', $pivot->quantity) }}">
            @error('quantity')
            <div class="alert alert-danger mt-2">{{ $message }}</div>
            @enderror
        </div>

        <button type="submit" class="btn btn-primary">Update product's quantity</button>
    </form>
@endsection

@extends('layouts.app')

@section('title', "Products ({$products->total()})")

@section('content')
    @if(auth()->user()->isAdmin())
        <a class="btn btn-primary float-end mb-4" href="{{ route('products.create') }}">Create a new product</a>
    @endif

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

    <table class="table">
        <thead>
            <tr>
                <th>Name</th>
                <th>Price</th>
                <th>Category</th>
                <th style="width: 650px">Description</th>
                <th>Centers</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{ $product->name }}</td>
                    <td>{{ $product->price }} €</td>
                    <td>{{ $product->category }}</td>
                    <td>{{ $product->description }}</td>
                    <td>
                        @foreach($product->centers as $center)
                            <a href="{{ route('centers.show', $center->id) }}">{{ $center->name }}</a>
                            <span class="badge rounded-pill bg-success">{{ $center->pivot->quantity }}</span>
                            <br>
                        @endforeach
                    </td>
                    <td>
                        <a class="btn btn-outline-secondary btn-sm" href="{{ route('products.show', $product->id) }}">Show</a>
                        @if(auth()->user()->isAdmin())
                            <a class="btn btn-secondary btn-sm" href="{{ route('products.edit', $product->id) }}">Edit</a>
                            <form class="d-inline" action="{{ route('products.destroy', $product->id) }}" method="POST">
                                @csrf
                                @method('DELETE')
                                <input class="btn btn-danger btn-sm" type="submit" value="Delete">
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    {{ $products->links("pagination::bootstrap-5") }}
@endsection

@extends('layouts.app')

@section('title', $product->name)

@section('content')
    <div class="row">
        <div class="col-12">
            <p>{{ $product->description }}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <p><b>Price:</b> {{ $product->price }}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <p><b>Category:</b> {{ $product->category }}</p>
        </div>
    </div>

    @if(auth()->user()->isAdmin())
        <div class="row">
            <div class="col-12">
                <a href="{{ route('products.edit', $product->id) }}" class="btn btn-primary">Edit</a>
            </div>
        </div>
    @endif
@endsection

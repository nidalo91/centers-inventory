@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-4"></div>
        <form action="{{ route('login') }}" method="POST" class="col-md-4">
            @csrf

            <h4>Log In to your account</h4>

            <div class="mb-3">
                <label for="login" class="form-label">Login</label>
                <input type="text" class="form-control" id="login" name="login" value="{{ old('login') }}">
                @error('login')
                    <div class="alert alert-danger mt-2">{{ $message }}</div>
                @enderror
            </div>

            <div class="mb-3">
                <label for="password" class="form-label">Password</label>
                <input type="password" class="form-control" id="password" name="password">
                @error('password')
                    <div class="alert alert-danger mt-2">{{ $message }}</div>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary">Login</button>
        </form>
        <div class="col-md-4"></div>
    </div>
@endsection

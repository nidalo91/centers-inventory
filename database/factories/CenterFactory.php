<?php

declare(strict_types=1);

namespace Database\Factories;

use Faker\Provider\fr_FR\Address;
use Faker\Provider\fr_FR\Company;
use Illuminate\Database\Eloquent\Factories\Factory;

final class CenterFactory extends Factory
{
    public function definition(): array
    {
        $this->faker->addProvider(new Company($this->faker));
        $this->faker->addProvider(new Address($this->faker));

        return [
            'name' => "Centre {$this->faker->unique()->numberBetween(1, 10)}",
            'address' => $this->faker->address,
            'city' => $this->faker->city,
            'state' => $this->faker->city,
            'zip' => $this->faker->postcode,
        ];
    }
}

<?php

declare(strict_types=1);

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

final class TransferFactory extends Factory
{
    public function definition(): array
    {
        return [
            'sender_id' => $this->faker->numberBetween(1, 5),
            'receiver_id' => $this->faker->numberBetween(6, 10),
            'product_id' => $this->faker->numberBetween(1, 100),
            'quantity' => $this->faker->numberBetween(1, 5),
        ];
    }
}

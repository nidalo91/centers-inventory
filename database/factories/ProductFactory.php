<?php

declare(strict_types=1);

namespace Database\Factories;

use App\Enums\ProductCategory;
use Illuminate\Database\Eloquent\Factories\Factory;

final class ProductFactory extends Factory
{
    public function definition(): array
    {
        return [
            'name' => "Product {$this->faker->randomNumber(2, true)}",
            'description' => $this->faker->text(100),
            'price' => $this->faker->randomFloat(2, 0, 100),
            'category' => $this->faker->randomElement(array_column(ProductCategory::cases(), 'name')),
        ];
    }
}

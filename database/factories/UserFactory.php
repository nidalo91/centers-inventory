<?php

declare(strict_types=1);

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

final class UserFactory extends Factory
{
    public function definition(): array
    {
        return [
            'login' => $this->faker->unique()->userName,
            'password' => 'password',
            'center_id' => $this->faker->numberBetween(1, 10),
        ];
    }
}

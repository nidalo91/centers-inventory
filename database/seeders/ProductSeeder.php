<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

final class ProductSeeder extends Seeder
{
    public function run(): void
    {
        $products = Product::factory()
            ->count(100)
            ->create();

        $products->each(function (Product $product) {
            $product->centers()->attach(
                rand(1, 10),
                [
                    'quantity' => rand(5, 100),
                ]
            );
        });
    }
}

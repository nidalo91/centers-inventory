<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\Center;
use Illuminate\Database\Seeder;

final class CenterSeeder extends Seeder
{
    public function run(): void
    {
        Center::factory()
            ->count(10)
            ->create();
    }
}

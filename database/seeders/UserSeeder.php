<?php

declare(strict_types=1);

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

final class UserSeeder extends Seeder
{
    public function run(): void
    {
        User::create([
            'login' => 'admin',
            'password' => 'password',
            'center_id' => 1,
            'is_admin' => true,
        ]);

        User::create([
            'login' => 'user',
            'password' => 'password',
            'center_id' => 2,
            'is_admin' => false,
        ]);

        User::factory()
            ->count(8)
            ->create();
    }
}

<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\CenterProductController;
use App\Http\Controllers\CentersController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\TransfersController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

Route::get('/login', [LoginController::class, 'show'])->name('login')->middleware('guest');
Route::post('/login', [LoginController::class, 'login'])->middleware('guest');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/', HomeController::class)->name('home');
    Route::resource('centers', CentersController::class);
    Route::resource('products', ProductsController::class);
    Route::resource('centers.products', CenterProductController::class)->except(['index', 'show']);
    Route::resource('transfers', TransfersController::class)->except(['show', 'edit', 'destroy']);
    Route::resource('users', UsersController::class)->except(['show', 'edit', 'update']);
    Route::post('/logout', LogoutController::class)->name('logout');
});
